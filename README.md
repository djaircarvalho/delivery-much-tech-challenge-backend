
# Delivery Much Tech Challenge

## Description

A simple solution for `Delivery Much Tech Challenge` using Nestjs.

##Configuration

Copy the `.env.saple` file to `.env` and update the giphy secret if you want to.

```bash
# Running the app
  $ docker-compose up
```

```bash
# unit tests
  $ npm run test
```

## Calling the api:

The app has only one endpoint: `http://127.0.0.1:3000/recipes`.
You should pass a list of ingredients as a query parameter to get a list of recipes.

```bash
  $ curl --request GET \
    --url 'http://127.0.0.1:3000/recipes/?i=onion,tomato'
```
