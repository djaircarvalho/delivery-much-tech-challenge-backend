import { Injectable } from '@nestjs/common'
import { RecipeDto } from 'src/recipe/dto'
import { GifFinder, RecipePuppyClient } from 'src/recipe/wrapper'
import { Recipe } from 'src/recipe/domain'

@Injectable()
export class RecipeService {
  constructor(
    private readonly recipePuppyClient: RecipePuppyClient,
    private readonly gifFinder: GifFinder,
  ) {}

  async findFor(ingredients: string[]): Promise<RecipeDto> {
    const foundRecipes = await this.recipePuppyClient.recipesFor(ingredients)
    const recipesWrapper = new RecipeDto()
    recipesWrapper.keywords = ingredients
    recipesWrapper.recipes = await this.enrichReceipts(foundRecipes)
    return recipesWrapper
  }

  async enrichReceipts(foundRecipes: Recipe[]): Promise<Recipe[]> {
    return Promise.all(
      foundRecipes.map(recipe => {
        return this.findGifFor(recipe).then(gif => {
          recipe.gif = gif
          return recipe
        })
      }),
    )
  }

  private async findGifFor(recipe: Recipe): Promise<string> {
    return await this.gifFinder.find(recipe.title)
  }
}
