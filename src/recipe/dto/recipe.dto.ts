import { Recipe } from 'src/recipe/domain'

export class RecipeDto {
  keywords: string[]
  recipes: Recipe[]
}
