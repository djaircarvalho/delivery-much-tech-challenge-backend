import {
  BadRequestException,
  Controller,
  Get,
  InternalServerErrorException,
  Query,
  ServiceUnavailableException,
  UnprocessableEntityException,
} from '@nestjs/common'
import { RecipeService } from 'src/recipe/service'
import { RecipeDto } from 'src/recipe/dto'

@Controller('recipes')
export class RecipeController {
  private INGREDIENTS_MAXIMUN_SIZE = 3

  constructor(private readonly recipeService: RecipeService) {}

  @Get()
  async findRecepes(@Query('i') query: string): Promise<RecipeDto> {
    const ingredients: string[] = query && query.split(',')

    this.validateIngredients(ingredients)
    try {
      const recipes = await this.recipeService.findFor(ingredients)
      return recipes
    } catch (error) {
      throw new ServiceUnavailableException(error.message)
    }
  }

  private validateIngredients(ingredients: string[]): void {
    this.validateMinimunIngredients(ingredients)
    this.validateMaximunIngredients(ingredients)
  }

  private validateMinimunIngredients(ingredients: string[]): void {
    if (!ingredients) throw new BadRequestException('At least one ingredient should be informed')
  }

  private validateMaximunIngredients(ingredients: string[]): void {
    if (ingredients.length > this.INGREDIENTS_MAXIMUN_SIZE) {
      throw new BadRequestException(
        `Ingredients size can't be greater than ${this.INGREDIENTS_MAXIMUN_SIZE}`,
      )
    }
  }
}
