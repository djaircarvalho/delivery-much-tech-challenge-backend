import { Expose, Transform } from 'class-transformer'

export class Recipe {
  @Expose()
  title: string

  @Expose()
  @Transform(value => value.split(',').map(value => value.trim()).sort())
  ingredients: string[]

  @Expose({ name: 'href' })
  link: string

  @Expose()
  gif: string
}
