import { Inject, Injectable, Logger } from '@nestjs/common'

import * as GphApiClient from 'giphy-js-sdk-core'
import { GifFinderError } from 'src/recipe/wrapper/errors'

@Injectable()
export class GifFinder {
  private logger: Logger
  private DEFAULT_GIF: string = 'https://media.giphy.com/media/hXD3cypLkycW1hQTFz/giphy.gif'

  constructor(private readonly gphApiClient: GphApiClient) {
    this.logger = new Logger(GifFinder.name)
  }

  async find(query: string): Promise<string> {
    try {
      return await this.searchGif(query)
    } catch (error) {
      this.logger.error(`Error getting the gif: ${error.message}`)
      throw new GifFinderError('Error getting the gif')
    }
  }

  private async searchGif(query: string): Promise<string> {
    const response = await this.gphApiClient.search('gifs', { q: query, limit: 1 })

    return response.data[0]?.images?.original?.url || this.DEFAULT_GIF
  }
}
