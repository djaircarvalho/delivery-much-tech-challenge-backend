import { HttpService, Injectable, Logger } from '@nestjs/common'
import { Recipe } from 'src/recipe/domain'
import { plainToClass } from 'class-transformer'
import { RecipePuppyClientError } from 'src/recipe/wrapper/errors'

const PUPPY_URL = 'http://www.recipepuppy.com/api'

@Injectable()
export class RecipePuppyClient {
  private logger: Logger

  constructor(private readonly httpService: HttpService) {
    this.logger = new Logger(RecipePuppyClient.name)
  }

  async recipesFor(ingredients: string[]): Promise<Recipe[]> {
    try {
      const result = await this.httpService.get(`${PUPPY_URL}?i=${ingredients.join()}`).toPromise()

      return result.data.results.map(recipe =>
        plainToClass(Recipe, recipe, { excludeExtraneousValues: true }),
      )
    } catch (error) {
      this.logger.error(`Error getting the recipes: ${error.message}`)
      throw new RecipePuppyClientError('Error getting the recipes')
    }
  }
}
