import { HttpModule, Module } from '@nestjs/common'
import { RecipeController } from 'src/recipe/controller'
import { RecipeService } from 'src/recipe/service'
import { GifFinder, RecipePuppyClient } from 'src/recipe/wrapper'
import * as GphApiClient from 'giphy-js-sdk-core'
@Module({
  imports: [HttpModule],
  controllers: [RecipeController],
  providers: [
    RecipeService,
    RecipePuppyClient,
    GifFinder,
    { provide: GphApiClient, useFactory: () => GphApiClient(process.env.GIPHY_KEY) },
  ],
})
export class RecipeModule {}
