import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'

import { RecipeModule } from 'src/recipe/recipe.module'

@Module({
  imports: [
    RecipeModule,
    ConfigModule.forRoot({
      isGlobal: true,
    }),
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
