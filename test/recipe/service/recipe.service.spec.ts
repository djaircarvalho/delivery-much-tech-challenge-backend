import { HttpModule } from '@nestjs/common'
import { Test, TestingModule } from '@nestjs/testing'
import { RecipeService } from 'src/recipe/service'
import { GifFinder, RecipePuppyClient } from 'src/recipe/wrapper'
import * as GphApiClient from 'giphy-js-sdk-core'
import { RecipeModule } from 'src/recipe/recipe.module'

describe('RecipeService', () => {
  let service: RecipeService
  let recipePuppyClient: RecipePuppyClient
  let gifFinder: GifFinder

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule, RecipeModule],
      providers: [RecipeService, RecipePuppyClient, GifFinder, GphApiClient],
    }).compile()

    service = module.get<RecipeService>(RecipeService)
    recipePuppyClient = module.get<RecipePuppyClient>(RecipePuppyClient)
    gifFinder = module.get<GifFinder>(GifFinder)
  })

  it('Should call both puppy and giphy apis', async () => {
    const recipePuppyClientSpy = jest
      .spyOn(recipePuppyClient, 'recipesFor')
      .mockImplementationOnce(() =>
        Promise.resolve([{ title: 'some title', link: 'the://link', gif: null, ingredients: [] }]),
      )
    const giphyfinderSpy = jest
      .spyOn(gifFinder, 'find')
      .mockImplementationOnce(() => Promise.resolve(''))
    await service.findFor(['some stuff'])

    expect(recipePuppyClientSpy).toHaveBeenCalledWith(['some stuff'])
    expect(giphyfinderSpy).toHaveBeenCalledWith('some title')
  })
})
