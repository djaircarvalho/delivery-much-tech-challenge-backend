import { Test, TestingModule } from '@nestjs/testing'
import * as request from 'supertest'
import { RecipeController } from 'src/recipe/controller'
import { HttpModule, INestApplication } from '@nestjs/common'
import { RecipeService } from 'src/recipe/service'
import { GifFinder, RecipePuppyClient } from 'src/recipe/wrapper'
import * as GphApiClient from 'giphy-js-sdk-core'

describe('RecipeController', () => {
  let app: INestApplication
  let controller: RecipeController
  let service: RecipeService

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      controllers: [RecipeController],
      providers: [RecipeService, RecipePuppyClient, GifFinder, GphApiClient],
    }).compile()

    app = module.createNestApplication()
    await app.init()
    controller = module.get<RecipeController>(RecipeController)
    service = module.get<RecipeService>(RecipeService)
  })

  it('(GET) withingredients should be ok', () => {
    const result = {
      keywords: ['onion', 'tomato'],
      recipes: [
        {
          title: 'Greek Omelet with Feta',
          ingredients: ['garlic', 'red onions', 'tomato', 'water'],
          link: 'http://www.kraftfoods.com/kf/recipes/greek-omelet-feta-104508.aspx',
          gif: 'https://media.giphy.com/media/xBRhcST67lI2c/giphy.gif',
        },
      ],
    }
    jest.spyOn(service, 'findFor').mockImplementationOnce(() => Promise.resolve(result))
    return request(app.getHttpServer())
      .get('/recipes?i=onion,tomato')
      .expect(200)
      .expect({
        keywords: ['onion', 'tomato'],
        recipes: [
          {
            title: 'Greek Omelet with Feta',
            ingredients: ['garlic', 'red onions', 'tomato', 'water'],
            link: 'http://www.kraftfoods.com/kf/recipes/greek-omelet-feta-104508.aspx',
            gif: 'https://media.giphy.com/media/xBRhcST67lI2c/giphy.gif',
          },
        ],
      })
  })

  it('(GET) with zero ingredients should return an error', () => {
    return request(app.getHttpServer())
      .get('/recipes')
      .expect(400)
      .expect({
        statusCode: 400,
        message: 'At least one ingredient should be informed',
        error: 'Bad Request',
      })
  })

  it('(GET) with more than 3 ingredients should return an error', () => {
    return request(app.getHttpServer())
      .get('/recipes?i=ingredient_1,ingredient_2,ingredient_3,ingredient_4')
      .expect(400)
      .expect({
        statusCode: 400,
        message: "Ingredients size can't be greater than 3",
        error: 'Bad Request',
      })
  })
})
