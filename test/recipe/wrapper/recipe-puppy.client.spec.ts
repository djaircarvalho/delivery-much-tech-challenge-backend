import { HttpService } from '@nestjs/common'
import { of } from 'rxjs'
import { RecipePuppyClient } from 'src/recipe/wrapper'

describe('RecipePuppyClient', () => {
  let client: RecipePuppyClient
  let httpService

  beforeEach(async () => {
    httpService = new HttpService()
    client = new RecipePuppyClient(httpService)
  })

  describe('getRecipes', () => {
    it('map all fields', async () => {
      const response = {
        headers: {},
        config: {},
        status: 200,
        statusText: 'OK',
        data: {
          title: 'Recipe Puppy',
          version: 0.1,
          href: 'http://www.recipepuppy.com/',
          results: [
            {
              title: 'Ginger Champagne',
              href: 'http://allrecipes.com/Recipe/Ginger-Champagne/Detail.aspx',
              ingredients: 'champagne, ginger, ice, vodka',
              thumbnail: 'http://img.recipepuppy.com/1.jpg',
            },
          ],
        },
      }
      jest.spyOn(httpService, 'get').mockImplementationOnce(() => of(response))

      const result = await client.recipesFor(['champagne'])

      expect(result[0].title).toEqual('Ginger Champagne')
      expect(result[0].link).toEqual('http://allrecipes.com/Recipe/Ginger-Champagne/Detail.aspx')
      expect(result[0].ingredients).toEqual(['champagne', 'ginger', 'ice', 'vodka'])
    })
  })

  it('throw an error on any error', async () => {
    jest.spyOn(httpService, 'get').mockImplementationOnce(() => {
      throw new Error('Some error')
    })

    try {
      await client.recipesFor(['champagne'])
    } catch (e) {
      expect(e.message).toEqual('Error getting the recipes')
    }
  })
})
