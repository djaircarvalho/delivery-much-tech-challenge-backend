import { HttpService } from '@nestjs/common'
import { of } from 'rxjs'
import { GifFinder } from 'src/recipe/wrapper'

describe('GifFinder', () => {
  let gifFinder: GifFinder
  let giphyMock = {
    search: () => ({
      data: [
        {
          images: {
            original: {
              url: 'expected',
            },
          },
        },
      ],
    }),
  }

  beforeEach(async () => {
    gifFinder = new GifFinder(giphyMock)
  })

  describe('getRecipes', () => {
    it('return an url on success', async () => {
      const result = await gifFinder.find('champagne')
      expect(result).toEqual('expected')
    })

    it('throw an error on any error', async () => {
      jest.spyOn(giphyMock, 'search').mockImplementationOnce(() => {
        throw new Error('Some error')
      })

      try {
        await gifFinder.find('champagne')
      } catch (e) {
        expect(e.message).toEqual('Error getting the gif')
      }
    })

    it('return dfault gif when gif no found', async () => {
      giphyMock = {
        search: () => ({
          data: [],
        }),
      }
      gifFinder = new GifFinder(giphyMock)

      const result = await gifFinder.find('champagne')
      expect(result).toEqual('https://media.giphy.com/media/hXD3cypLkycW1hQTFz/giphy.gif')
    })
  })
})
